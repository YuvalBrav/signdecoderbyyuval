# SignLanguage

## Getting started

1. Clone the repo:
```
git clone https://gitlab.com/YuvalBrav/signdecoderbyyuval.git
```
2. After cloning this git you need to run:
```
python Main.py
```
3. You might need to download some libraries, such as: OpenCv, Numpy, Tensorflow.

4. In order to user the program you need a Webcam and Python v3.7.4 or higher.

## Introduction
American Sign Language (ASL) is a natural language[4] that serves as the predominant sign language of Deaf communities in the United States and most of Anglophone Canada. ASL is a complete and organized visual language that is expressed by facial expression as well as movements and motions with the hands.

ASL shown below:

![ASL](https://raw.githubusercontent.com/VedantMistry13/American-Sign-Language-Recognition-using-Deep-Neural-Network/master/images/NIDCD-ASL-hands-2014.jpg "ASL")

## Contact
Yuval Braverman - yuvalbr10@gmail.com


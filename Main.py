import Camera #the Camera.py file
import GUI #the GUI.py file
import Model #the Model.py file
import cv2
import shutil
import os
import mediapipe as mp
from os import path

mp_hands = mp.solutions.hands
num_prediction = 0

def main():
    global num_prediction
    letter_predictions = {}
    # the number '0' representing the connection(camera) number
    # it's important to know because we need to connect to the right input of the user
    prediction_final = "put hand in frame"
    cap = cv2.VideoCapture(0)

    # removes old picture of hands + making folder for the photos of the hands
    #if path.exists('hands_pics'):
    #    shutil.rmtree('hands_pics')
    #os.mkdir('hands_pics')

    window, picture_place, prediction_place = GUI.start_GUI()
    hands_model, classes = Model.open_model()
    with mp_hands.Hands(model_complexity=0, min_detection_confidence=0.5, min_tracking_confidence=0.5,
                        max_num_hands=2) as model:
        while cap.isOpened():
            image, image_to_predict = Camera.open_camera(model, cap)

            GUI.change_image(window, image, picture_place, prediction_place, prediction_final)

            if image_to_predict != "None":
                prediction = Model.predict(image_to_predict, hands_model, classes)
                letter_predictions = Model.add_prediction_to_dictionary(letter_predictions, prediction)
                num_prediction += 1

                if num_prediction == 10:
                    num_prediction = 0
                    prediction_final = Model.prediction_of_letter(letter_predictions)
                    print(prediction_final)
                    letter_predictions = {}

            if not GUI.RUNNING:
                break
        cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
import numpy as np
from PIL import Image, ImageOps

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# Prepare data generator for standardizing frames before sending them into the model.
data_generator = ImageDataGenerator(samplewise_center=True, samplewise_std_normalization=True)

# Setting up the input image size and frame crop size.
IMAGE_SIZE = 200

def open_model():
    """
    :return: return the model and the classes that the model can give predict on.
    """
    MODEL_NAME = 'models/asl_alphabet_2.h5'
    model = load_model(MODEL_NAME)

    # Creating list of available classes stored in classes.txt.
    classes_file = open("models/classes.txt")
    classes_string = classes_file.readline()
    classes = classes_string.split()
    classes.sort()  # The predict function sends out output in sorted order.
    return model, classes


def predict(image_to_predict, model, classes):
    """
    :param image_to_predict: the image from the user cam
    :param model: the model itself
    :param classes: the classes that the model cangive predict on
    :return: prediction of the model with it's probability
    """
    prediction = ""

    #Add border
    image = Image.fromarray(image_to_predict)
    image = ImageOps.expand(image, border=2, fill = "#0302f3")
    image = image.resize((IMAGE_SIZE,IMAGE_SIZE),Image.ANTIALIAS)

    #Transform to numpyarr
    imgarr = np.array(image)
    imgarr = (np.array(imgarr)).reshape((1, IMAGE_SIZE, IMAGE_SIZE, 3))
    frame_for_model = data_generator.standardize(np.float64(imgarr))

    # Predicting the frame.
    prediction = np.array(model.predict(frame_for_model))
    predicted_class = classes[prediction.argmax()]  # Selecting the max confidence index.


    # Preparing output based on the model's confidence.
    prediction_probability = prediction[0, prediction.argmax()]
    if prediction_probability > 0.5:
        # High confidence.
        prediction = '{} - {:.2f}'.format(predicted_class, prediction_probability * 100)
    elif prediction_probability > 0.2 and prediction_probability <= 0.5:
        # Low confidence.
        prediction = '{} - {:.2f}'.format(predicted_class, prediction_probability * 100)
    else:
        # No confidence.
        prediction = classes[-2]

    return prediction


def prediction_of_letter(dict_predictions):
    """
    Gets the dict of last 10 predictions and gives the final prediction (which will be shown to user).
    Does this by the avg of the letters predicted + num times each letter was predicted
    :param dict_predictions: dict type, contain keys of letters and values of lists, that contain all the percentage predictions to that letter.
    :return: string of prediction - the letter the program thinks the user is showing
    """
    letter_of_max_times = []
    max_times = 0
    best_avg = 0
    predicted_letter = 'nothing'

    # Find which letter/s is/are the most predicted
    for key in dict_predictions.keys():
        if len(dict_predictions[key]) > max_times:
            max_times = len(dict_predictions[key])
            letter_of_max_times = [key]
        elif len(dict_predictions[key]) == max_times:
            letter_of_max_times += [key]

    # if there are 2 or more that are the most predicted
    if len(letter_of_max_times) > 1:
        for letter in letter_of_max_times:
            percentage_list = dict_predictions[letter]
            avg = sum(percentage_list) / len(percentage_list)
            if avg > best_avg:
                best_avg = avg
                predicted_letter = letter
    # Only 1 best, then taking the avg of the predictions percentages
    elif len(letter_of_max_times) == 1:
        percentage_list = dict_predictions[letter_of_max_times[0]]
        best_avg = sum(percentage_list) / len(percentage_list)
        predicted_letter = letter_of_max_times[0]

    if predicted_letter == 'nothing':
        return '{}'.format(predicted_letter)
    else:
        return '{} - {:.2f}'.format(predicted_letter, best_avg)


def add_prediction_to_dictionary(dict_predictions, prediction):
    """
    Add to the dict of predictions (given) the new prediction (given) and returns the updated dict.
    :param dict_predictions: dict of all the prediction. Keys are letters, values are lists of prediction's percentage (so number of predictions of letter is the size of the array).
    :param prediction: string of the letter and percentage of its accuracy. ('prediction' - 'percentage')
    :return: dict_predictions updated with the new prediction.
    """

    prediction = prediction.split(' ')
    if prediction[0] != "nothing":
        letter_prediction = prediction[0]
        if letter_prediction not in dict_predictions:
            dict_predictions[letter_prediction] = [float(prediction[-1])]
        else:
            old_prediction_array = dict_predictions[letter_prediction]
            old_prediction_array += [float(prediction[-1])]
            dict_predictions[letter_prediction] = old_prediction_array
    return dict_predictions

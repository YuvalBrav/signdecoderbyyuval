import cv2
import mediapipe as mp
import numpy as np
import matplotlib.pyplot as plt
import os
from os import path
from PIL import Image

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles

NUMBER = 0
IMAGE_SIZE = 200

def crop_image(image, coordinates, hand_index):
    """
    This function crop the hand from the image
    :param image: The whole image from the user cam
    :param coordinates: The coordinates we want to crop from the whole image
    :param hand_index: The hand we want to crop - Left/Right
    :return: Croped image or "None" if there is no hand in the pic or if the whole hand is not in it
    """
    global NUMBER
    x1, y1, x2, y2 = coordinates

    # if all hand in photo, then crop photo
    if not (x1 < 0 or y1 < 0 or x2 > image.shape[1] or y2 > image.shape[0]):
        crop_img = image[y1:y2, x1:x2]
        
        crop_img = cv2.cvtColor(crop_img, cv2.COLOR_BGR2RGB)

        # if the hand is the left hand - flip it
        if hand_index == 'Left Hand':
            crop_img = np.flip(crop_img, 1)

        return crop_img

    return "None"

def mediapipe_detection(image, model):
    """
    :param image: The whole image from the user cam
    :param model: media pip model
    :return: The image and the result of the prediction
    """
    # cv2 gives as the photo as bgr we need to make it rgb
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # Image is no longer writeable
    image.flags.writeable = False

    # Make prediction
    results = model.process(image)
    image.flags.writeable = True

    # COLOR COVERSION RGB 2 BGR
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return image, results


def draw_bounding_boxes(image, results, hand_status, padd_amount=10, draw=True, display=True):
    """
    This function draw bounding boxes for both hands and give the coordinates to 'crop_image'
    :param image: The whole image from the user cam
    :param results: The results from mediapipe prediction
    :param hand_status: Right or Left hand
    :return: output_image- the image with the bounding boxes
    image_to_predict- The image to give prediction on
    """
    # Create a copy of the input image to draw bounding boxes on and write hands types labels.
    output_image = image.copy()

    # Initialize a dictionary to store both (left and right) hands landmarks as different elements.
    output_landmarks = {}

    # Get the height and width of the input image.
    height, width, _ = image.shape

    # Iterate over the found hands.
    for hand_index, hand_landmarks in enumerate(results.multi_hand_landmarks):

        # Initialize a list to store the detected landmarks of the hand.
        landmarks = []

        # Iterate over the detected landmarks of the hand.
        for landmark in hand_landmarks.landmark:
            # Append the landmark into the list.
            landmarks.append((int(landmark.x * width), int(landmark.y * height),
                              (landmark.z * width)))

        # Get all the x-coordinate values from the found landmarks of the hand.
        x_coordinates = np.array(landmarks)[:, 0]

        # Get all the y-coordinate values from the found landmarks of the hand.
        y_coordinates = np.array(landmarks)[:, 1]

        coordinates = []
        # Get the bounding box coordinates for the hand with the specified padding.
        x1 = int(np.min(x_coordinates) - padd_amount)
        y1 = int(np.min(y_coordinates) - padd_amount)
        x2 = int(np.max(x_coordinates) + padd_amount)
        y2 = int(np.max(y_coordinates) + padd_amount)

        # Check if coordinates are showing a square. If not, then make it
        if(x2-x1) < (y2-y1):
            x2 += (y2-y1) - (x2-x1)
        if(x2-x1) > (y2-y1):
            y2 += (x2-x1) - (y2-y1)
        # Jump in 25 pixels each time
        x2 += 25 - ((x2-x1) % 25)
        y2 += 25 - ((y2-y1) % 25)

        # If too small, don't enter
        if (x2-x1) >= 50:
            coordinates.append(x1)
            coordinates.append(y1)
            coordinates.append(x2)
            coordinates.append(y2)

            # Initialize a variable to store the label of the hand.
            label = "Unknown"

            # Check if the hand we are iterating upon is the right one.
            if hand_status['Right_index'] == hand_index:

                # Update the label and store the landmarks of the hand in the dictionary.
                label = 'Right Hand'
                output_landmarks['Right'] = landmarks

            # Check if the hand we are iterating upon is the left one.
            elif hand_status['Left_index'] == hand_index:

                # Update the label and store the landmarks of the hand in the dictionary.
                label = 'Left Hand'
                output_landmarks['Left'] = landmarks

            # crop the hand out of the picture
            image_to_predict = crop_image(image, coordinates, label)

            # Check if the bounding box and the classified label is specified to be written.
            if draw:
                # Draw the bounding box around the hand on the output image.
                cv2.rectangle(output_image, (x1, y1), (x2, y2), (155, 0, 255), 3, cv2.LINE_8)

                # Write the classified label of the hand below the bounding box drawn.
                cv2.putText(output_image, label, (x1, y2 + 25), cv2.FONT_HERSHEY_COMPLEX, 0.7, (20, 255, 155), 1,
                            cv2.LINE_AA)
        else:
            print("Get closer :)")

    # Check if the output image is specified to be displayed.
    if display:

        # Display the output image.
        plt.figure(figsize=[10, 10])
        plt.title("Output Image");
        plt.axis('off');

    # Otherwise
    else:
        # Return the output image and the landmarks dictionary.
        return output_image, image_to_predict


def get_hand_type(image, results, draw=True, display=True):
    """
    Gives the hand type
    :param image: The whole image from the user cam
    :param results: The results from the mediapipe model
    :return: hands_status - the hand type
    """

    # Initialize a dictionary to store the classification info of both hands.
    hands_status = {'Right': False, 'Left': False, 'Right_index': None, 'Left_index': None}

    # Iterate over the found hands in the image.
    for hand_index, hand_info in enumerate(results.multi_handedness):

        # Retrieve the label of the found hand.
        hand_type = hand_info.classification[0].label

        # Update the status of the found hand.
        hands_status[hand_type] = True

        # Update the index of the found hand.
        hands_status[hand_type + '_index'] = hand_index

        # Check if the hand type label is specified to be written.
        if draw:
            # Write the hand type on the output image.
            cv2.putText(image, hand_type + ' Hand Detected', (10, (hand_index + 1) * 30), cv2.FONT_HERSHEY_PLAIN,
                        2, (0, 255, 0), 2)

    # Check if the output image is specified to be displayed.
    if display:
        plt.figure(figsize=[10, 10])
        plt.title("Output Image");
        plt.axis('off');

    # Otherwise
    else:

        # Return the output image and the hands status dictionary that contains classification info.
        return hands_status



def open_camera(model, cap):
    ret, frame = cap.read()

    # Flip the frame horizontally for natural (selfie-view) visualization.
    frame = cv2.flip(frame, 1)
    image, results = mediapipe_detection(frame, model)

    # Draw bounding boxes around the detected hands and write their classified types near them.
    if results.multi_hand_landmarks != None:
        hands_status = get_hand_type(frame.copy(), results, draw=False, display=False)
        image, image_to_predict = draw_bounding_boxes(frame, results, hands_status, display=False)
        return (image, image_to_predict)

    return (image, "None")
from tkinter import *
from PIL import ImageTk, Image
import cv2

RUNNING = TRUE

def start_GUI():
    mywindow = Tk() #Change the name for every window you make
    mywindow.title("Sign Decoder") #This will be the window title
    mywindow.geometry("900x577")  # This will be the window size (str)
    mywindow.minsize(900, 577)  # This will be set a limit for the window's minimum size (int)
    mywindow['background'] = "#deeade"  # This will be the background color
    mywindow.iconbitmap("icon.ico") #Set icon

    mywindow.protocol("WM_DELETE_WINDOW", close_window)

    #video output
    label_frame = LabelFrame(mywindow)
    label_frame.pack()
    label_frame.place(x=20, y=76) #placement of video
    Picture = Label(label_frame)
    Picture.pack()

    #prediction output
    label_frame = LabelFrame(mywindow)
    label_frame.pack()
    label_frame.place(x=687, y=150) #placement of prediction
    prediction = Label(label_frame, width= 20, height= 20, font="railway")
    prediction.pack()

    #Logo
    logo = Image.open("Logo.png")
    logo = ImageTk.PhotoImage(logo)
    logo_label = Label(image=logo)
    logo_label.image = logo
    logo_label.place(relx=0.5, y=32, anchor='center')

    return mywindow, Picture, prediction

def change_image(mywindow, image, picture_label, prediction_label, prediction = ""):
    """
    This function changes the image in the GUI
    :param mywindow: Tkinter object of the GUI
    :param image: The current image
    :param picture_label: The label of the image in the screen(GUI)
    :param prediction_label: The label of the prediction in the screen(GUI)
    :param prediction: The prediction itself
    """
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # Creates a Tkinter-compatible photo image, which can be used everywhere Tkinter expects an image object.
    img = ImageTk.PhotoImage(image=Image.fromarray(image))

    prediction_label['text'] = prediction
    picture_label['image'] = img

    mywindow.update()

def close_window():
    """
    Close gracefully the gui
    """
    global RUNNING
    RUNNING = False  # turn off while loop
    print("Window closed")
